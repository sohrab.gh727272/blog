<x-panel-layout>
    <x-slot name="title">
        - مدیریت کاربران
    </x-slot>
    <div class="breadcrumb">
        <ul>
            <li><a href="{{route('dashboard')}}">پیشخوان</a></li>
            <li><a href="{{route('users.index')}}" class="is-active">کاربران</a></li>
        </ul>
    </div>
    <div class="main-content font-size-13">
        <div class="tab__box">
            <div class="tab__items">
                <a class="tab__item is-active" href="{{route('users.index')}}">همه کاربران</a>
                <a class="tab__item" href="{{route('users.create')}}">ایجاد کاربر جدید</a>
            </div>
        </div>
        <div class="d-flex flex-space-between item-center flex-wrap padding-30 border-radius-3 bg-white">
        </div>
        <div class="table__box">
            <table class="table">
                <thead role="rowgroup">
                <tr role="row" class="title-row">
                    <th>شناسه</th>
                    <th>نام و نام خانوادگی</th>
                    <th>ایمیل</th>
                    <th>سطح کاربری</th>
                    <th>تاریخ عضویت</th>
                    <th>وضعیت حساب</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $key=>$user)
                    <tr role="row" class="">
                        <td><a href="">{{$key+1}}</a></td>
                        <td><a href="">{{$user->name}}</a></td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->persian_roel}}</td>
                        <td>1399/11/11</td>
                        <td class="text-success">تاییده شده</td>
                        <td>
                            <a href="" class="item-delete mlg-15" title="حذف"></a>
                            <a href="" class="item-confirm mlg-15" title="تایید"></a>
                            <a href="" class="item-reject mlg-15" title="رد"></a>
                            <a href="{{route('users.edit',auth()->id())}}" class="item-edit " title="ویرایش"></a>
                        </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-panel-layout>
