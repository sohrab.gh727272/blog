<?php

use App\Http\Controllers\panel\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
})->name('landing');

Route::get('/dashboard', function () {
    return view('panel.index');
})->name('dashboard');
Route::get('/post/{id}', function ($id) {
    return view('post');
})->name('post.show');

Route::get('/profile', function () {
    return 'profile';
})->name('profile');
require __DIR__ . '/auth.php';

Route::resource('panel/users',UserController::class);
