<x-panel-layout>
    <x-slot name="title">
        ویرایش کاربر  -
    </x-slot>
    <div class="breadcrumb">
        <ul>
            <li><a href="{{route('dashboard')}}">پیشخوان</a></li>
            <li><a href="{{route('users.index')}}" class="is-active">کاربران</a></li>
            <li><a href="{{route('users.edit',auth()->id())}}" class="is-active">ویرایش کاربر جدید</a></li>
        </ul>
    </div>
    <div class="main-content font-size-13">
        <div class="row no-gutters  bg-white">
            <div class="col-12">
                <p class="box__title">ویرایش کاربر</p>
                <form action="{{route('users.store')}}" class="padding-30" method="post">
                    @csrf
                    <input name="name" type="text" class="text" placeholder="نام و نام خانوادگی">
                    <input name="email" type="email" class="text" placeholder="ایمیل">
                    <input name="mobile" type="text" class="text" placeholder="شماره موبایل">

                    <select name="role" id="" class="text">
                        <option value="0">کاربر عادی</option>
                        <option value="1">مدرس</option>
                        <option value="2">نویسنده</option>
                        <option value="3">مدیر</option>
                    </select>

                    <button class="btn btn-webamooz_net">ویرایش</button>
                </form>

            </div>
        </div>
    </div>
</x-panel-layout>
