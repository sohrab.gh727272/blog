<x-app-layout>
    <main class="bg--white">
        <div class="container">
            <div class="sign-page">
                <h1 class="sign-page__title">بازیابی رمز عبور</h1>

                <form class="sign-page__form" method="POST" action="{{route('password.email')}}">
                    @csrf
                    <input type="text" class="text text--left" name="email" placeholder="ایمیل">
                    @error('email')
                    <p style="margin-bottom: 1rem;
                    color: green;
                    text-align: right"
                    >{{$message}}</p>
                    @enderror
                    @if(Session::has('status'))
                        {{Session::get('status')}}
                    @endif
                    <button type="submit" class="btn btn--blue btn--shadow-blue width-100 ">بازیابی</button>
                    <div class="sign-page__footer">
                        <span>کاربر جدید هستید؟</span>
                        <a href="{{route('register')}}" class="color--46b2f0">صفحه ثبت نام</a>
                    </div>
                </form>
            </div>
        </div>
    </main>
</x-app-layout>
