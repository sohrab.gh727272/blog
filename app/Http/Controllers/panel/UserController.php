<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('panel.users.index', compact('users'));
    }

    public function create()
    {
        return view('panel.users.create');
    }

    public function edit()
    {
        return view('panel.users.edit');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['string', 'max:255','required'],
            'email' => ['email', 'unique:users','required'],
            'mobile' => ['string', 'unique:users','required'],
            'role' =>['string','required'],
        ]);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'role' => $request->role,
            'password' => Hash::make('password')
        ]);
        return redirect()->back();
    }
}
